﻿namespace WindowsFormsApplication2
{
    partial class FabCalc
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_squareRoot = new System.Windows.Forms.Button();
            this.btn_plusMinus = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.btn_clearall = new System.Windows.Forms.Button();
            this.btn_backspace = new System.Windows.Forms.Button();
            this.lbl_calculationSteps = new System.Windows.Forms.Label();
            this.btn_equalButton = new System.Windows.Forms.Button();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_dot = new System.Windows.Forms.Button();
            this.btn_zero = new System.Windows.Forms.Button();
            this.btn_percent = new System.Windows.Forms.Button();
            this.btn_divide = new System.Windows.Forms.Button();
            this.btn_nine = new System.Windows.Forms.Button();
            this.btn_subtract = new System.Windows.Forms.Button();
            this.btn_eight = new System.Windows.Forms.Button();
            this.btn_three = new System.Windows.Forms.Button();
            this.btn_seven = new System.Windows.Forms.Button();
            this.btn_two = new System.Windows.Forms.Button();
            this.btn_one = new System.Windows.Forms.Button();
            this.Button11 = new System.Windows.Forms.Button();
            this.btn_six = new System.Windows.Forms.Button();
            this.btn_five = new System.Windows.Forms.Button();
            this.btn_four = new System.Windows.Forms.Button();
            this.btn_multpy = new System.Windows.Forms.Button();
            this.CalculatorDisplay = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_squareRoot
            // 
            this.btn_squareRoot.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_squareRoot.Location = new System.Drawing.Point(173, 81);
            this.btn_squareRoot.Name = "btn_squareRoot";
            this.btn_squareRoot.Size = new System.Drawing.Size(35, 32);
            this.btn_squareRoot.TabIndex = 53;
            this.btn_squareRoot.Text = "√";
            this.btn_squareRoot.UseVisualStyleBackColor = true;
            // 
            // btn_plusMinus
            // 
            this.btn_plusMinus.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_plusMinus.Location = new System.Drawing.Point(132, 81);
            this.btn_plusMinus.Name = "btn_plusMinus";
            this.btn_plusMinus.Size = new System.Drawing.Size(35, 32);
            this.btn_plusMinus.TabIndex = 49;
            this.btn_plusMinus.Text = "±";
            this.btn_plusMinus.UseVisualStyleBackColor = true;
            this.btn_plusMinus.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calcOperation);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(91, 81);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(35, 32);
            this.btn_clear.TabIndex = 50;
            this.btn_clear.Text = "C";
            this.btn_clear.UseVisualStyleBackColor = true;
            // 
            // btn_clearall
            // 
            this.btn_clearall.Location = new System.Drawing.Point(50, 81);
            this.btn_clearall.Name = "btn_clearall";
            this.btn_clearall.Size = new System.Drawing.Size(35, 32);
            this.btn_clearall.TabIndex = 51;
            this.btn_clearall.Text = "CE";
            this.btn_clearall.UseVisualStyleBackColor = true;
            this.btn_clearall.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_backspace
            // 
            this.btn_backspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_backspace.Location = new System.Drawing.Point(9, 81);
            this.btn_backspace.Name = "btn_backspace";
            this.btn_backspace.Size = new System.Drawing.Size(35, 32);
            this.btn_backspace.TabIndex = 52;
            this.btn_backspace.Text = "←";
            this.btn_backspace.UseVisualStyleBackColor = true;
            this.btn_backspace.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // lbl_calculationSteps
            // 
            this.lbl_calculationSteps.AutoSize = true;
            this.lbl_calculationSteps.ForeColor = System.Drawing.SystemColors.ButtonShadow;
            this.lbl_calculationSteps.Location = new System.Drawing.Point(12, 26);
            this.lbl_calculationSteps.Name = "lbl_calculationSteps";
            this.lbl_calculationSteps.Size = new System.Drawing.Size(0, 13);
            this.lbl_calculationSteps.TabIndex = 0;
            this.lbl_calculationSteps.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btn_equalButton
            // 
            this.btn_equalButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_equalButton.Location = new System.Drawing.Point(172, 195);
            this.btn_equalButton.Name = "btn_equalButton";
            this.btn_equalButton.Size = new System.Drawing.Size(35, 70);
            this.btn_equalButton.TabIndex = 44;
            this.btn_equalButton.Text = "=";
            this.btn_equalButton.UseVisualStyleBackColor = true;
            this.btn_equalButton.Click += new System.EventHandler(this.equalButton_Click);
            // 
            // btn_add
            // 
            this.btn_add.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_add.Location = new System.Drawing.Point(131, 233);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(35, 32);
            this.btn_add.TabIndex = 45;
            this.btn_add.Text = "+";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calcOperation);
            // 
            // btn_dot
            // 
            this.btn_dot.Location = new System.Drawing.Point(90, 233);
            this.btn_dot.Name = "btn_dot";
            this.btn_dot.Size = new System.Drawing.Size(35, 32);
            this.btn_dot.TabIndex = 46;
            this.btn_dot.Text = ".";
            this.btn_dot.UseVisualStyleBackColor = true;
            this.btn_dot.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_zero
            // 
            this.btn_zero.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.btn_zero.Location = new System.Drawing.Point(9, 233);
            this.btn_zero.Name = "btn_zero";
            this.btn_zero.Size = new System.Drawing.Size(76, 32);
            this.btn_zero.TabIndex = 47;
            this.btn_zero.Text = "0";
            this.btn_zero.UseVisualStyleBackColor = true;
            this.btn_zero.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_percent
            // 
            this.btn_percent.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_percent.Location = new System.Drawing.Point(173, 119);
            this.btn_percent.Name = "btn_percent";
            this.btn_percent.Size = new System.Drawing.Size(35, 32);
            this.btn_percent.TabIndex = 35;
            this.btn_percent.Text = "%";
            this.btn_percent.UseVisualStyleBackColor = true;
            // 
            // btn_divide
            // 
            this.btn_divide.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_divide.Location = new System.Drawing.Point(132, 119);
            this.btn_divide.Name = "btn_divide";
            this.btn_divide.Size = new System.Drawing.Size(35, 32);
            this.btn_divide.TabIndex = 37;
            this.btn_divide.Text = "/";
            this.btn_divide.UseVisualStyleBackColor = true;
            this.btn_divide.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calcOperation);
            // 
            // btn_nine
            // 
            this.btn_nine.Location = new System.Drawing.Point(91, 119);
            this.btn_nine.Name = "btn_nine";
            this.btn_nine.Size = new System.Drawing.Size(35, 32);
            this.btn_nine.TabIndex = 39;
            this.btn_nine.Text = "9";
            this.btn_nine.UseVisualStyleBackColor = true;
            this.btn_nine.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_subtract
            // 
            this.btn_subtract.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_subtract.Location = new System.Drawing.Point(131, 195);
            this.btn_subtract.Name = "btn_subtract";
            this.btn_subtract.Size = new System.Drawing.Size(35, 32);
            this.btn_subtract.TabIndex = 36;
            this.btn_subtract.Text = "-";
            this.btn_subtract.UseVisualStyleBackColor = true;
            this.btn_subtract.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calcOperation);
            // 
            // btn_eight
            // 
            this.btn_eight.Location = new System.Drawing.Point(50, 119);
            this.btn_eight.Name = "btn_eight";
            this.btn_eight.Size = new System.Drawing.Size(35, 32);
            this.btn_eight.TabIndex = 40;
            this.btn_eight.Text = "8";
            this.btn_eight.UseVisualStyleBackColor = true;
            this.btn_eight.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_three
            // 
            this.btn_three.Location = new System.Drawing.Point(90, 195);
            this.btn_three.Name = "btn_three";
            this.btn_three.Size = new System.Drawing.Size(35, 32);
            this.btn_three.TabIndex = 38;
            this.btn_three.Text = "3";
            this.btn_three.UseVisualStyleBackColor = true;
            this.btn_three.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_seven
            // 
            this.btn_seven.Location = new System.Drawing.Point(9, 119);
            this.btn_seven.Name = "btn_seven";
            this.btn_seven.Size = new System.Drawing.Size(35, 32);
            this.btn_seven.TabIndex = 42;
            this.btn_seven.Text = "7";
            this.btn_seven.UseVisualStyleBackColor = true;
            this.btn_seven.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_two
            // 
            this.btn_two.Location = new System.Drawing.Point(49, 195);
            this.btn_two.Name = "btn_two";
            this.btn_two.Size = new System.Drawing.Size(35, 32);
            this.btn_two.TabIndex = 41;
            this.btn_two.Text = "2";
            this.btn_two.UseVisualStyleBackColor = true;
            this.btn_two.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_one
            // 
            this.btn_one.Location = new System.Drawing.Point(8, 195);
            this.btn_one.Name = "btn_one";
            this.btn_one.Size = new System.Drawing.Size(35, 32);
            this.btn_one.TabIndex = 43;
            this.btn_one.Text = "1";
            this.btn_one.UseVisualStyleBackColor = true;
            this.btn_one.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // Button11
            // 
            this.Button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Button11.Location = new System.Drawing.Point(173, 157);
            this.Button11.Name = "Button11";
            this.Button11.Size = new System.Drawing.Size(35, 32);
            this.Button11.TabIndex = 31;
            this.Button11.Text = "℅";
            this.Button11.UseVisualStyleBackColor = true;
            // 
            // btn_six
            // 
            this.btn_six.Location = new System.Drawing.Point(91, 157);
            this.btn_six.Name = "btn_six";
            this.btn_six.Size = new System.Drawing.Size(35, 32);
            this.btn_six.TabIndex = 32;
            this.btn_six.Text = "6";
            this.btn_six.UseVisualStyleBackColor = true;
            this.btn_six.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_five
            // 
            this.btn_five.Location = new System.Drawing.Point(50, 157);
            this.btn_five.Name = "btn_five";
            this.btn_five.Size = new System.Drawing.Size(35, 32);
            this.btn_five.TabIndex = 33;
            this.btn_five.Text = "5";
            this.btn_five.UseVisualStyleBackColor = true;
            this.btn_five.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_four
            // 
            this.btn_four.Location = new System.Drawing.Point(9, 157);
            this.btn_four.Name = "btn_four";
            this.btn_four.Size = new System.Drawing.Size(35, 32);
            this.btn_four.TabIndex = 34;
            this.btn_four.Text = "4";
            this.btn_four.UseVisualStyleBackColor = true;
            this.btn_four.MouseClick += new System.Windows.Forms.MouseEventHandler(this.numberPressed);
            // 
            // btn_multpy
            // 
            this.btn_multpy.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_multpy.Location = new System.Drawing.Point(132, 157);
            this.btn_multpy.Name = "btn_multpy";
            this.btn_multpy.Size = new System.Drawing.Size(35, 32);
            this.btn_multpy.TabIndex = 30;
            this.btn_multpy.Text = "*";
            this.btn_multpy.UseVisualStyleBackColor = true;
            this.btn_multpy.MouseClick += new System.Windows.Forms.MouseEventHandler(this.calcOperation);
            // 
            // CalculatorDisplay
            // 
            this.CalculatorDisplay.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CalculatorDisplay.Location = new System.Drawing.Point(7, 42);
            this.CalculatorDisplay.Name = "CalculatorDisplay";
            this.CalculatorDisplay.Size = new System.Drawing.Size(200, 30);
            this.CalculatorDisplay.TabIndex = 29;
            this.CalculatorDisplay.TabStop = false;
            this.CalculatorDisplay.Text = "0.";
            this.CalculatorDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(217, 24);
            this.menuStrip1.TabIndex = 54;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.quitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // quitToolStripMenuItem
            // 
            this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
            this.quitToolStripMenuItem.Size = new System.Drawing.Size(97, 22);
            this.quitToolStripMenuItem.Text = "Quit";
            this.quitToolStripMenuItem.Click += new System.EventHandler(this.quitToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // FabCalc
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(217, 274);
            this.Controls.Add(this.btn_squareRoot);
            this.Controls.Add(this.btn_plusMinus);
            this.Controls.Add(this.btn_clear);
            this.Controls.Add(this.btn_clearall);
            this.Controls.Add(this.btn_backspace);
            this.Controls.Add(this.lbl_calculationSteps);
            this.Controls.Add(this.btn_equalButton);
            this.Controls.Add(this.btn_add);
            this.Controls.Add(this.btn_dot);
            this.Controls.Add(this.btn_zero);
            this.Controls.Add(this.btn_percent);
            this.Controls.Add(this.btn_divide);
            this.Controls.Add(this.btn_nine);
            this.Controls.Add(this.btn_subtract);
            this.Controls.Add(this.btn_eight);
            this.Controls.Add(this.btn_three);
            this.Controls.Add(this.btn_seven);
            this.Controls.Add(this.btn_two);
            this.Controls.Add(this.btn_one);
            this.Controls.Add(this.Button11);
            this.Controls.Add(this.btn_six);
            this.Controls.Add(this.btn_five);
            this.Controls.Add(this.btn_four);
            this.Controls.Add(this.btn_multpy);
            this.Controls.Add(this.CalculatorDisplay);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(233, 313);
            this.MinimumSize = new System.Drawing.Size(233, 313);
            this.Name = "FabCalc";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Fab Calc";
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FabCalc_KeyPress);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button btn_squareRoot;
        internal System.Windows.Forms.Button btn_plusMinus;
        internal System.Windows.Forms.Button btn_clear;
        internal System.Windows.Forms.Button btn_clearall;
        internal System.Windows.Forms.Button btn_backspace;
        internal System.Windows.Forms.Label lbl_calculationSteps;
        internal System.Windows.Forms.Button btn_equalButton;
        internal System.Windows.Forms.Button btn_add;
        internal System.Windows.Forms.Button btn_dot;
        internal System.Windows.Forms.Button btn_zero;
        internal System.Windows.Forms.Button btn_percent;
        internal System.Windows.Forms.Button btn_divide;
        internal System.Windows.Forms.Button btn_nine;
        internal System.Windows.Forms.Button btn_subtract;
        internal System.Windows.Forms.Button btn_eight;
        internal System.Windows.Forms.Button btn_three;
        internal System.Windows.Forms.Button btn_seven;
        internal System.Windows.Forms.Button btn_two;
        internal System.Windows.Forms.Button btn_one;
        internal System.Windows.Forms.Button Button11;
        internal System.Windows.Forms.Button btn_six;
        internal System.Windows.Forms.Button btn_five;
        internal System.Windows.Forms.Button btn_four;
        internal System.Windows.Forms.Button btn_multpy;
        internal System.Windows.Forms.TextBox CalculatorDisplay;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
    }
}

