﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class FabCalc : Form
    {
        Boolean calculationStarted; bool nextCalcStep = false, decimalInUse = false;
        string operation = "", operationPressed = "";

        // declare some variables that will help in creating some calculations
        private double numberBuffer1, numberBuffer2, numberBuffer3;

        public double NumberBuffer1
        {
            get
            {
                return numberBuffer1;
            }

            set
            {
                numberBuffer1 = value;
            }
        }

        public double NumberBuffer2
        {
            get
            {
                return numberBuffer2;
            }

            set
            {
                numberBuffer2 = value;
            }
        }

        public double NumberBuffer3
        {
            get
            {
                return numberBuffer3;
            }

            set
            {
                numberBuffer3 = value;
            }
        }

        public FabCalc()
        {
            InitializeComponent();
        }

        private void equalButton_Click(object sender, EventArgs e)
        {
            double answer;
            switch (operation)
            {
                case "+":
                    answer = NumberBuffer1 + Convert.ToDouble(CalculatorDisplay.Text);
                    // set the answer to the display
                    CalculatorDisplay.Text = "";
                    CalculatorDisplay.Text = answer.ToString();
                    // reset operation flag
                    operationPressed = "";
                    operation = "";
                    break;
                case "-":
                    answer = NumberBuffer1 - Convert.ToDouble(CalculatorDisplay.Text);
                    // set the answer to the display
                    CalculatorDisplay.Text = "";
                    CalculatorDisplay.Text = answer.ToString();
                    // reset operation flag
                    operationPressed = "";
                    operation = "";
                    break;
                case "*":
                    answer = NumberBuffer1 * Convert.ToDouble(CalculatorDisplay.Text);
                    // set the answer to the display
                    CalculatorDisplay.Text = "";
                    CalculatorDisplay.Text = answer.ToString();
                    // reset operation flag
                    operationPressed = "";
                    operation = "";
                    break;
                case "/":
                    answer = NumberBuffer1 / Convert.ToDouble(CalculatorDisplay.Text);
                    // set the answer to the display
                    CalculatorDisplay.Text = "";
                    CalculatorDisplay.Text = answer.ToString();
                    // reset operation flag
                    operationPressed = "";
                    operation = "";
                    break;
                default:
                    // nothing will happen if the button has been pressed without the assignment of the operation flag
                    break;
            }
            // label information
            lbl_calculationSteps.Text = "";

            // sort out the decimal being able to be used again.
            decimalInUse = false;
        }

        public void numberPressed(Object sender, MouseEventArgs e)
        {
            // convert the sender into the type of object that invoked it.
            Button btn = (Button)sender;

            String btnPressed = btn.Text;

            switch (btnPressed)
            {
                case "1":
                    addToCalcDisplay("1");
                    break;
                case "2":
                    addToCalcDisplay("2");
                    break;
                case "3":
                    addToCalcDisplay("3");
                    break;
                case "4":
                    addToCalcDisplay("4");
                    break;
                case "5":
                    addToCalcDisplay("5");
                    break;
                case "6":
                    addToCalcDisplay("6");
                    break;
                case "7":
                    addToCalcDisplay("7");
                    break;
                case "8":
                    addToCalcDisplay("8");
                    break;
                case "9":
                    addToCalcDisplay("9");
                    break;
                case "0":
                    // check to see if only 0 is in the display before adding another 0
                    if (CalculatorDisplay.Text == "0" | CalculatorDisplay.Text == "0.")
                    {
                        // do nothing
                    }
                    else
                    {
                        addToCalcDisplay("0");
                    }

                    break;
                case "←":
                    {
                        // remove the last number from the screen
                        int textSize = CalculatorDisplay.Text.Length - 1;

                        // check to see if the display is clear otr not. 0. means clear.
                        if (CalculatorDisplay.Text.Equals("0."))
                        {
                            // find the size of the string in the display.                     
                            MessageBox.Show("Can't remove anymore");
                            // set the display to the calcdisplay minus 1 char
                        }
                    }
                    break;
                case "CE":
                    // clear entry
                    CalculatorDisplay.Text = "0.";
                    // reset screen calculation started
                    calculationStarted = false;
                    break;
                case ".":
                    // if decimal is used then dont use another 
                    if (decimalInUse == false)
                    {
                        addToCalcDisplay(".");
                        decimalInUse = true;
                    }
                    else if (CalculatorDisplay.Text.Contains("."))
                    {
                        MessageBox.Show("Do nothing");
                    }

                    break;
            }
        }
        
        private void quitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // quit the application
            Application.Exit();
        }

        public void addToCalcDisplay(string valueToAdd)
        {
            if (CalculatorDisplay.Text == "0." || operationPressed != "")
            {
                // Then we are starting the entry of numbers.
                CalculatorDisplay.Clear();
            }
            CalculatorDisplay.Text = CalculatorDisplay.Text + valueToAdd;
            operationPressed = "";
        }

        private void calcOperation(object sender, MouseEventArgs e)
        {
            // convert sender into a button.
            Button pressedBtn = (Button)sender;

            // extract the name of the button
            string operationSelected = pressedBtn.Name.ToString();
            // press simulate calculation completing by pressing equals
            btn_equalButton.PerformClick();
            // decide what to based on the selection that was pressed.
            switch (operationSelected)
            {
                case "btn_add":
                    {
                        // add calculation
                        // add number on screen to buffer 
                        NumberBuffer1 = Convert.ToDouble(CalculatorDisplay.Text);
                        operationPressed = "+";

                        operation = "+";
                    }
                    break;
                case "btn_subtract":
                    {
                        // subtract calculation
                        // add number on screen to buffer 
                        NumberBuffer1 = Convert.ToDouble(CalculatorDisplay.Text);
                        operationPressed = "-";

                        operation = "-";
                    }
                    break;
                case "btn_multpy":
                    {
                        // times calculation
                        // add number on screen to buffer 
                        NumberBuffer1 = Convert.ToDouble(CalculatorDisplay.Text);
                        operationPressed = "*";

                        operation = "*";
                    }
                    break;
                case "btn_divide":
                    {
                        // devide calculation
                        // add number on screen to buffer 
                        NumberBuffer1 = Convert.ToDouble(CalculatorDisplay.Text);
                        operationPressed = "/";

                        operation = "/";
                    }
                    break;
                case "btn_clear":
                    {
                        // Clear calculation
                        lbl_calculationSteps.Text.Equals("");
                        NumberBuffer1 = 0;
                        CalculatorDisplay.Text = "0.";
                    }
                    break;
                case "btn_plusMinus":
                    {
                        // Clear calculation
                        lbl_calculationSteps.Text.Equals("-");

                        double tempNumber = Convert.ToDouble(CalculatorDisplay.Text);

                        if (tempNumber > 0)
                        {
                            tempNumber = -tempNumber;
                        }
                        else
                        {
                            tempNumber = +tempNumber;
                        }

                        CalculatorDisplay.Clear();
                        CalculatorDisplay.Text = tempNumber.ToString();

                        /* firstly get the number from the display, if it is 0 then do nothing.
                            if it is greater than 0 - the exact value from it to make it minus
                            if it is less than 0 then add the same value to it.
                        */
                    }
                    break;
            }
            lbl_calculationSteps.Text = NumberBuffer1 + " " + operation + " ";

        }
        private void FabCalc_KeyPress(object sender, KeyPressEventArgs e)
        {
            MessageBox.Show(e.KeyChar.ToString());
            switch (e.KeyChar.ToString())
            {
                case "1":
                    {
                        btn_one.PerformClick();
                    }
                    break;
                case "2":
                    {
                        btn_two.PerformClick();
                    }
                    break;
                case "3":
                    {
                        btn_three.PerformClick();
                    }
                    break;
                case "4":
                    {
                        btn_four.PerformClick();
                    }
                    break;
                case "5":
                    {
                        btn_five.PerformClick();
                    }
                    break;
                case "6":
                    {
                        btn_six.PerformClick();
                    }
                    break;
                case "7":
                    {
                        btn_seven.PerformClick();
                    }
                    break;
                case "8":
                    {
                        btn_eight.PerformClick();
                    }
                    break;
                case "9":
                    {
                        btn_nine.PerformClick();
                    }
                    break;
                case "0":
                    {
                        btn_zero.PerformClick();
                    }
                    break;
            }
        }
    }
}